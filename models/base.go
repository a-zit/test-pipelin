package models

import (
	"github.com/GoAdminGroup/go-admin/modules/db"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	orm *gorm.DB
	err error
)

func Init(c db.Connection) {
	orm, err = gorm.Open(mysql.New(mysql.Config{
		Conn: c.GetDB("default"),
	}), &gorm.Config{})

	if err != nil {
		panic("initialize orm failed")
	}

	MigratorAdmin()
	MigratorBranch()
	MigratorBrand()
	MigratorBrandLevel()
	MigratorBroadcastLog()
	MigratorBroadcastLog()
	MigratorUser()
	MigratorReadMessageLog()
	MigratorReadMessageLog()
	MigratorReward()
	MigratorRewardBranch()
	MigratorRewardLog()
}
