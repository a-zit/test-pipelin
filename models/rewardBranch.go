package models

import "time"

type RewardBranch struct {
	ID        uint   `gorm:"primaryKey"`
	RewardID  int    `gorm:"not null"`
	Reward    Reward `gorm:"foreignKey:RewardID"`
	BranchID  int    `gorm:"not null"`
	Branch    Branch `gorm:"foreignKey:BranchID"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func MigratorRewardBranch() {
	orm.AutoMigrate(&RewardBranch{})
}
