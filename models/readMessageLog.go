package models

import "time"

type ReadMessageLog struct {
	ID             uint `gorm:"primaryKey"`
	Count          *int
	BroadcastLogID int          `gorm:"not null"`
	BroadcastLog   BroadcastLog `gorm:"foreignKey:BroadcastLogID"`
	UserID         int          `gorm:"not null"`
	User           User         `gorm:"foreignKey:UserID"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
}

func MigratorReadMessageLog() {
	orm.AutoMigrate(ReadMessageLog{})
}
