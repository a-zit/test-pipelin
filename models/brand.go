package models

import (
	"time"

	"gorm.io/gorm"
)

type Brand struct {
	ID         uint   `gorm:"primaryKey"`
	Name       string `gorm:"not null"`
	BrandImage *string
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  gorm.DeletedAt `gorm:"index"`
}

func MigratorBrand() {
	orm.AutoMigrate(&Brand{})
}
