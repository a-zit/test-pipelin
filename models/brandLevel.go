package models

import "time"

type BrandLevel struct {
	Id              uint   `gorm:"primaryKey"`
	LevelName       string `gorm:"not null"`
	NumberCondition int    `gorm:"not null"`
	BrandID         int    `gorm:"not null"`
	Brand           Brand  `gorm:"foreignKey:BrandID"`
	CreatedAt       time.Time
	UpdatedAt       time.Time
}

func MigratorBrandLevel() {
	orm.AutoMigrate(&BrandLevel{})
}
