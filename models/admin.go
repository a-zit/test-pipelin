package models

import (
	"time"
)

type Admin struct {
	ID            uint `gorm:"primaryKey"`
	FirstName     *string
	LastName      *string
	Username      string `gorm:"not null;unique"`
	Password      string `gorm:"not null"`
	Avatar        *string
	Email         string `gorm:"unique;not null"`
	Tel           string `gorm:"unique;not null"`
	RememberToken *string
	BrandID       *int
	Brand         Brand `gorm:"foreignKey:BrandID"`
	BranchID      *int
	Branch        Branch `gorm:"foreignKey:BranchID"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

func MigratorAdmin() {
	orm.AutoMigrate(&Admin{})
}

func (admin *Admin) CreateAdmin() error {
	result := orm.Create(&admin)

	if result.Error != nil {
		return result.Error
	}

	return nil
}
