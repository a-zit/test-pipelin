package models

import (
	"database/sql/driver"
	"time"

	"github.com/golang-jwt/jwt"
)

type User struct {
	ID            uint     `gorm:"primaryKey"`
	UID           *string  `gorm:"unique"`
	Type          UserType `gorm:"type:ENUM('PARTNER', 'NORMAL')"`
	FirstName     string   `gorm:"not null"`
	LastName      string   `gorm:"not null"`
	Email         string   `gorm:"not null;unique"`
	Password      string   `gorm:"not null"`
	Token         string
	Tel           string `gorm:"not null;unique"`
	Level         *string
	Birthdate     *time.Time
	TotalPoint    int     `gorm:"default:0"`
	TotalScore    int     `gorm:"default:0"`
	OtpCode       *string `gorm:"size:6"`
	OtpVerifiedAt *time.Time
	OtpExpiredAt  *time.Time
	BrandID       int   `gorm:"not null"`
	Brand         Brand `gorm:"foreignKey:BrandID"`
	jwt.StandardClaims
	CreatedAt time.Time
	UpdatedAt time.Time
}

type UserType string

const (
	Partner UserType = "PARTNER"
	Normal  UserType = "NORMAL"
)

func (e *UserType) Scan(value interface{}) error {
	*e = UserType(value.([]byte))
	return nil
}

func (e UserType) Value() (driver.Value, error) {
	return string(e), nil
}

func MigratorUser() {
	orm.AutoMigrate(&User{})
}
