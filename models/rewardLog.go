package models

import "time"

type RewardLog struct {
	ID         uint `gorm:"primaryKey"`
	Detail     *string
	Code       *string
	RedeemedAt *time.Time
	BranchID   int    `gorm:"not null"`
	Branch     Branch `gorm:"foreignKey:BranchID"`
	RewardID   int    `gorm:"not null"`
	Reward     Reward `gorm:"foreignKey:RewardID"`
	UserID     int    `gorm:"not null"`
	User       User   `gorm:"foreignKey:UserID"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

func MigratorRewardLog() {
	orm.AutoMigrate(&RewardLog{})
}
