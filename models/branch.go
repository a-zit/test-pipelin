package models

import (
	"time"

	"gorm.io/gorm"
)

type Branch struct {
	ID          uint   `gorm:"primaryKey"`
	Name        string `gorm:"not null"`
	BranchImage *string
	Latitude    *float64
	Longitude   *float64
	BrandID     int   `gorm:"not null"`
	Brand       Brand `gorm:"foreignKey:BrandID"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt `gorm:"index"`
}

func MigratorBranch() {
	orm.AutoMigrate(&Branch{})
}
