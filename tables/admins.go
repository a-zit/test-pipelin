package tables

import (
	"atmosph-crm-be/models"
	"strconv"

	"github.com/GoAdminGroup/go-admin/context"
	"github.com/GoAdminGroup/go-admin/modules/db"
	form2 "github.com/GoAdminGroup/go-admin/plugins/admin/modules/form"
	"github.com/GoAdminGroup/go-admin/plugins/admin/modules/table"
	"github.com/GoAdminGroup/go-admin/template/types"
	"github.com/GoAdminGroup/go-admin/template/types/form"
	"golang.org/x/crypto/bcrypt"
)

type AdminTableService struct {
	Admin models.Admin
}

func (ats *AdminTableService) GetAdminsTable(ctx *context.Context) table.Table {

	admins := table.NewDefaultTable(table.DefaultConfigWithDriver("mysql"))

	info := admins.GetInfo().HideFilterArea()

	info.AddField("ID", "id", db.Bigint).FieldSortable()
	info.AddField("First Name", "first_name", db.Longtext).FieldDisplay(func(models types.FieldModel) interface{} {
		var result = ""
		if models.Value == "" {
			result = "-"
		} else {
			result = models.Value
		}
		return result
	})
	info.AddField("Last Name", "last_name", db.Longtext).FieldDisplay(func(models types.FieldModel) interface{} {
		var result = ""
		if models.Value == "" {
			result = "-"
		} else {
			result = models.Value
		}
		return result
	})
	info.AddField("Username", "username", db.Varchar)
	info.AddField("Avatar", "avatar", db.Longtext)
	info.AddField("Email", "email", db.Varchar)
	info.AddField("Tel", "tel", db.Varchar)
	info.AddField("Role Name", "name", db.Varchar).FieldJoin(types.Join{
		Table:     "goadmin_role_users",
		Field:     "id",
		JoinField: "user_id",
	}).FieldJoin(types.Join{
		BaseTable: "goadmin_role_users",
		Table:     "goadmin_roles",
		Field:     "role_id",
		JoinField: "id",
	})
	info.AddField("Brand", "name", db.Varchar).FieldJoin(types.Join{
		Table:     "brands",
		Field:     "brand_id",
		JoinField: "id",
	})
	info.AddField("Branch", "name", db.Varchar).FieldJoin(types.Join{
		Table:     "branches",
		Field:     "branch_id",
		JoinField: "id",
	})

	info.SetTable("admins").SetTitle("Admins").SetDescription("Admins")

	formList := admins.GetForm()
	formList.AddField("First_name", "first_name", db.Longtext, form.Text)
	formList.AddField("Last_name", "last_name", db.Longtext, form.Text)
	formList.AddField("Username", "username", db.Varchar, form.Text)
	formList.AddField("Password", "password", db.Varchar, form.Password)
	// formList.AddField("Password Confirmation", "password_confirmation", db.Varchar, form.Password)
	// .
	// 	FieldPostFilterFn(func(value types.PostFieldModel) interface{} {
	// 		var password = value.Value.Value()
	// 		passwordByte := []byte(password)
	// 		passwordBcrypt, _ := bcrypt.GenerateFromPassword(passwordByte, 60)
	// 		return passwordBcrypt
	// 	})
	// formList.AddField("Avatar", "avatar", db.Longtext, form.File)
	formList.AddField("Email", "email", db.Varchar, form.Email)
	formList.AddField("Tel", "tel", db.Varchar, form.Text)
	formList.AddField("Brand", "brand_id", db.Int, form.SelectSingle).
		FieldOptionsFromTable("brands", "name", "id").
		FieldHelpMsg("Please select your brand")

	formList.SetInsertFn(func(values form2.Values) error {
		firstName := values.Get("first_name")
		lastName := values.Get("last_name")
		username := values.Get("username")
		password := values.Get("password")
		hash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		email := values.Get("email")
		tel := values.Get("tel")
		brandId := values.Get("brand_id")
		convBrandId, _ := strconv.Atoi(brandId)

		ats.Admin.FirstName = &firstName
		ats.Admin.LastName = &lastName
		ats.Admin.Username = username
		ats.Admin.Password = string(hash)
		ats.Admin.Tel = tel
		ats.Admin.Email = email
		ats.Admin.BrandID = &convBrandId
		err := ats.Admin.CreateAdmin()
		return err
	})
	formList.SetTable("admins").SetTitle("Admins").SetDescription("Admins")

	return admins
}
