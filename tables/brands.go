package tables

import (
	"github.com/GoAdminGroup/go-admin/context"
	"github.com/GoAdminGroup/go-admin/modules/db"
	"github.com/GoAdminGroup/go-admin/plugins/admin/modules/table"
	"github.com/GoAdminGroup/go-admin/template/types/form"
)

func GetBrandsTable(ctx *context.Context) table.Table {

	brands := table.NewDefaultTable(table.DefaultConfigWithDriver("mysql"))

	info := brands.GetInfo().HideFilterArea()

	info.AddField("Id", "id", db.Bigint).
		FieldFilterable()
	info.AddField("Name", "name", db.Longtext)
	info.AddField("Brand_image", "brand_image", db.Longtext)
	// info.AddField("Created_at", "created_at", db.Datetime)
	// info.AddField("Updated_at", "updated_at", db.Datetime)
	// info.AddField("Deleted_at", "deleted_at", db.Datetime)

	info.SetTable("brands").SetTitle("Brands").SetDescription("Brands")

	formList := brands.GetForm()
	// formList.AddField("Id", "id", db.Bigint, form.Default)
	formList.AddField("Name", "name", db.Longtext, form.RichText)
	formList.AddField("Brand_image", "brand_image", db.Longtext, form.RichText)
	// formList.AddField("Created_at", "created_at", db.Datetime, form.Datetime)
	// formList.AddField("Updated_at", "updated_at", db.Datetime, form.Datetime)
	// formList.AddField("Deleted_at", "deleted_at", db.Datetime, form.Datetime)

	formList.SetTable("brands").SetTitle("Brands").SetDescription("Brands")

	return brands
}
